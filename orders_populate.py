import requests
import json
import sys
import time
import gspread
import numpy
from oauth2client.client import SignedJwtAssertionCredentials


def send_request(payload, url):
    headers = {
        'content-type': "application/json",
        'cache-control': "no-cache",
        'referer': "https://www.urbanladder.com"
        }

    response = requests.request("POST", url, data=payload, headers=headers, allow_redirects=False)
    return response

def get_content(filename):
    file = open(filename)
    content = file.read()
    file.close()
    return content

def iterate(number, url, filename):
    response_summary = []
    for i in range(number):
        payload = get_content(filename)
        response = send_request(payload, url)
        if(response.status_code != 302):
            response_summary.append({'code': response.status_code, 'time': response.elapsed.total_seconds(), 'iteration': (i + 1), 'error': 'Wrong status code. Exiting program...'})
            print(response_summary)
            exit(1)
        response_summary.append({'code': response.status_code, 'time': response.elapsed.total_seconds(), 'iteration': (i + 1)})
    return response_summary

def write_to_csv(response_summary, filename, url):
    time_of_execution = time.strftime("%a %d %b %Y %H:%M:%S +0530", time.localtime())
    name = filename.split('/')[-1].split('.')[0] + " in host "+ url.split('://')[-1].split('/')[0]
    file = open(name+".csv", "a+")

    for r in response_summary:
        string = str(time_of_execution) + ','+str(r['code'])+','+str(r['time'])+','+str(r['iteration'])+"\n"
        file.write(string)
    file.close()

def write_to_google_sheet(response_summary, filename, domain, number_of_iterations, secrets_path, time_of_execution, tag):
    json_key = json.load(open(secrets_path))
    scope = ['https://spreadsheets.google.com/feeds']
    credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'], scope)
    gc = gspread.authorize(credentials)
    sheet_name = filename.split('/')[-1].split('.')[0] + '-' +domain.split('://')[-1].split('.')[0]
    spreadsheet = gc.open_by_key('1cnogtSrtgoSyPd2oYORQiaIqOCsn1MlDth_r6883Vgo')
    print(sheet_name)
    sheet = spreadsheet.worksheet(sheet_name)
    times = [];
    for r in response_summary:
        times.append(r['time'])
    values = [domain, time_of_execution, numpy.std(times), number_of_iterations, numpy.sum(times), tag]
    print(values)
    sheet.insert_row(values, 2)
    print('append successful')

time_of_execution = time.strftime("%d-%m-%Y %H:%M %Z", time.localtime())
domain = str(sys.argv[2])
url = domain + "/orders/populate"
filename = str(sys.argv[1])
number_of_iterations = int(sys.argv[3])
secrets_path = sys.argv[4]
print(secrets_path)
tag = str(sys.argv[5])
response_summary = iterate(number_of_iterations, url, filename)
write_to_csv(response_summary, filename, url)
print(response_summary)
write_to_google_sheet(response_summary, filename, domain, number_of_iterations, secrets_path, time_of_execution, tag)
