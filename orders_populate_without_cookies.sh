#!/bin/bash
total=0;
for ((i=1;i<="$2";i++)); do
  response="$(curl -w'{"code": "%{http_code}", "response_time": "%{time_total}"}' -s -X POST -H "Content-Type: application/json" -H "Cache-Control: no-cache" -d "@$1" "$3/orders/populate" -o /dev/null)";
  echo $response;
  code="$(echo "${response}" | jq .code)";
  expected_code="302";
  if [[ "$code" != "302" ]]; then
    echo "error - $code";
  else
    response_time="$(echo "${response}" | jq .response_time)";
    total="$(echo "${total} + ${response_time}" | bc)";
  fi
done
avg="$(echo "${total} / $2" | bc -l)";
echo "----------------------------------------------------";
echo "                      Summary                       ";
echo "----------------------------------------------------";
echo "              Total Time : ${total}                 ";
echo "    Number of Iterations : $2                       ";
echo "            Average Time :$avg                      ";
echo "----------------------------------------------------";




# #!/bin/bash
# total=0;
# for ((i=0;i<"$2";i++)); do
#   response=$(curl -w"{code: %{http_code}, response_time: %{time_total}}" -s -X POST -H "Content-Type: application/json" -H "Cache-Control: no-cache" -d "@$1" "$3/orders/populate" -o "log/orders_populate_without_cookies.log");
#   echo "$response";
#   if [ "$response|jq .code" = "302" ];then
#     echo "$response|jq.response_time";
#   else
#     exit 1;
#   fi
# done
#
# # avg="$(echo "${total} / $2" | bc -l)";
# # echo "----------------------------------------------------";
# # echo "                      Summary                       ";
# # echo "----------------------------------------------------";
# # echo "              Total Time : ${total}                 ";
# # echo "    Number of Iterations : $2                       ";
# # echo "            Average Time :$avg                      ";
# # echo "----------------------------------------------------";
