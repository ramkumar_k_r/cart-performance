import requests
import json
import csv
import sys
import gspread
from oauth2client.client import SignedJwtAssertionCredentials

filename = str(sys.argv[1])
domain = str(sys.argv[2]).split('://')[-1].split('.')[0]
secrets_path = str(sys.argv[3])
tag = str(sys.argv[4])
json_key = json.load(open(secrets_path))
scope = ['https://spreadsheets.google.com/feeds']
credentials = SignedJwtAssertionCredentials(json_key['client_email'], json_key['private_key'], scope)
gc = gspread.authorize(credentials)

spreadsheet = gc.open_by_key('1cnogtSrtgoSyPd2oYORQiaIqOCsn1MlDth_r6883Vgo')
sheet = spreadsheet.worksheet('backburner_log')
file = open(filename)
rows = csv.reader(file)
for row in rows:
    row.append(domain)
    row.append(tag)
    sheet.insert_row(row, 2)
file.delete()
print('apppend successful')
