#!/bin/bash
host=$1
if [[ "$host" == "http://localhost:3000" ]]; then
  #statements
  path="/Users/ramkumar/ul2/hercules/"
  cp $path/log/promotion_calculation_job.csv ./backburner_log/
else
  path="/vol/ume01/srv/www/hercules/current"
  ssh -n ramkumarkr@$1 "sudo -u root mv ${path}/log/promotion_calculation_job.csv /home/ramkumarkr"
  scp ramkumarkr@$1:/home/ramkumarkr/promotion_calculation_job.csv backburner_log/
fi
